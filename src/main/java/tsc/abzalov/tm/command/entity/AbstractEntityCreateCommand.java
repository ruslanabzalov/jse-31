package tsc.abzalov.tm.command.entity;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.function.Supplier;

import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public abstract class AbstractEntityCreateCommand<E extends AbstractBusinessEntity> extends AbstractEntityCommand<E> {

    public AbstractEntityCreateCommand(@NotNull final IServiceLocator serviceLocator,
                                       @NotNull final Supplier<E> supplier, @NotNull final Class<E> clazz) {
        super(serviceLocator, supplier, clazz);
    }

    @Override
    public void execute() {
        System.out.println("ENTITY CREATION");
        @NotNull val name = inputName();
        @NotNull val description = inputDescription();

        @Nullable val entity = getEntity();
        if (entity == null) return; // TODO: Выбрасывать исключение вместо return.

        entity.setName(name);
        entity.setDescription(description);
        entity.setUserId(getAuthService().getCurrentUserId());

        if (entity instanceof Project) getServiceLocator().getProjectService().create((Project) entity);
        if (entity instanceof Task) getServiceLocator().getTaskService().create((Task) entity);

        System.out.println("Entity was successfully created.\n");
    }

}
