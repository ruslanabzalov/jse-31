package tsc.abzalov.tm.command.entity;

import lombok.AccessLevel;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

import java.util.function.Supplier;

public abstract class AbstractEntityCommand<E extends AbstractBusinessEntity> extends AbstractCommand {

    @Nullable
    private final Supplier<E> supplier;

    @NotNull
    @Getter(value = AccessLevel.PROTECTED)
    private final String typeName;

    @NotNull
    @Getter(value = AccessLevel.PROTECTED)
    private final IAuthService authService = getServiceLocator().getAuthService();

    public AbstractEntityCommand(@NotNull final IServiceLocator serviceLocator,
                                 @Nullable final Supplier<E> supplier, @NotNull final Class<E> clazz) {
        super(serviceLocator);
        this.supplier = supplier;
        this.typeName = clazz.getCanonicalName();
    }

    @Nullable
    protected E getEntity() {
        if (supplier == null) return null;
        return supplier.get();
    }

}
