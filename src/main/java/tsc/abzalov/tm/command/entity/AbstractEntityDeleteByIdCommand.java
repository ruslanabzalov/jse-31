package tsc.abzalov.tm.command.entity;

import lombok.var;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import static tsc.abzalov.tm.util.InputUtil.inputId;

public abstract class AbstractEntityDeleteByIdCommand<E extends AbstractBusinessEntity>
        extends AbstractEntityCommand<E> {

    public AbstractEntityDeleteByIdCommand(@NotNull final IServiceLocator serviceLocator,
                                           @NotNull final Class<E> clazz) {
        super(serviceLocator, null, clazz);
    }

    @Override
    public void execute() {
        System.out.println("DELETE ENTITY BY ID");

        var areEntitiesExist = false;
        if (getTypeName().equals(Project.class.getCanonicalName()))
            areEntitiesExist = getServiceLocator().getProjectService().size(getAuthService().getCurrentUserId()) != 0;
        if (getTypeName().equals(Task.class.getCanonicalName()))
            areEntitiesExist = getServiceLocator().getTaskService().size(getAuthService().getCurrentUserId()) != 0;

        if (areEntitiesExist) {
            if (getTypeName().equals(Project.class.getCanonicalName()))
                getServiceLocator().getProjectService().removeById(getAuthService().getCurrentUserId(), inputId());
            if (getTypeName().equals(Task.class.getCanonicalName()))
                getServiceLocator().getTaskService().removeById(getAuthService().getCurrentUserId(), inputId());
            System.out.println("Entity was successfully deleted.\n");
            return;
        }

        System.out.println("Entities are empty.\n");
    }

}
