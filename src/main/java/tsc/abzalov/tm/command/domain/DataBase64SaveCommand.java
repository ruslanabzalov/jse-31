package tsc.abzalov.tm.command.domain;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;
import static tsc.abzalov.tm.util.SystemUtil.BASE64_ENCODER;

@SuppressWarnings("unused")
public final class DataBase64SaveCommand extends AbstractDomainCommand {

    public DataBase64SaveCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-base64-save";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data in Base64 format.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(BASE64_FILENAME);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull val byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull val objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        val domainBytes = byteArrayOutputStream.toByteArray();
        val base64Domain = BASE64_ENCODER.encode(domainBytes);

        @NotNull val fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64Domain);
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("Data was saved in Base64 format.\n");
    }

}
