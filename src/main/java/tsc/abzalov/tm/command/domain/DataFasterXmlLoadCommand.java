package tsc.abzalov.tm.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.File;
import java.io.FileNotFoundException;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataFasterXmlLoadCommand extends AbstractDomainCommand {

    public DataFasterXmlLoadCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-fasterxml-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from XML format via FasterXML.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val xmlMapper = new XmlMapper().registerModule(new JavaTimeModule());
        @NotNull val file = new File(FASTERXML_XML_FILENAME);

        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val domain = xmlMapper.readValue(file, Domain.class);
        setDomain(domain);

        System.out.println("Data was loaded from XML format via FasterXML.\nPlease, re-login.\n");
    }

}
