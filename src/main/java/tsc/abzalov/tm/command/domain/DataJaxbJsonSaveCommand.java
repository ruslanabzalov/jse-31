package tsc.abzalov.tm.command.domain;

import lombok.SneakyThrows;
import lombok.val;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataJaxbJsonSaveCommand extends AbstractDomainCommand {

    public DataJaxbJsonSaveCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-jaxb-json-save";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data in JSON format via JAXB.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty(JAXB_CONTEXT_FACTORY_PROPERTY_NAME, JAXB_CONTEXT_FACTORY_PROPERTY_VALUE);

        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, JAXB_MEDIA_TYPE);
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        @NotNull val file = new File(JAXB_JSON_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val domain = getDomain();
        marshaller.marshal(domain, file);

        System.out.println("Data was saved in JSON format via JAXB.\n");
    }

}
