package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

@SuppressWarnings("unused")
public final class ProjectUpdateByIdCommand extends AbstractCommand {

    public ProjectUpdateByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "update-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("EDIT PROJECT BY ID");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull val projectId = inputId();
            @NotNull val projectName = inputName();
            @NotNull val projectDescription = inputDescription();
            System.out.println();

            @Nullable val project =
                    projectService.editById(currentUserId, projectId, projectName, projectDescription);
            val wasProjectEdited = Optional.ofNullable(project).isPresent();
            if (wasProjectEdited) {
                System.out.println("Project was successfully updated.\n");
                return;
            }

            System.out.println("Project was not updated! Please, check that project exists and try again.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
