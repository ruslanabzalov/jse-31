package tsc.abzalov.tm.command.system;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

@SuppressWarnings("unused")
public final class SystemArgumentsCommand extends AbstractCommand {

    public SystemArgumentsCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "arguments";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all available arguments.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val commandService = serviceLocator.getCommandService();
        @NotNull val arguments = commandService.getCommandArguments();

        arguments.forEach(System.out::println);
        System.out.println();
    }

}
