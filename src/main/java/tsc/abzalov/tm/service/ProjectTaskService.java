package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository,
                              @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    @SneakyThrows
    public int indexOf(@Nullable String userId, @Nullable Task task) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        task = Optional.ofNullable(task).orElseThrow(EmptyEntityException::new);
        return taskRepository.indexOf(userId, task);
    }

    @Override
    @SneakyThrows
    public boolean hasData(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return projectRepository.size(userId) != 0 && taskRepository.size(userId) != 0;
    }

    @Override
    @SneakyThrows
    public void addTaskToProjectById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        taskId = Optional.ofNullable(taskId).orElseThrow(EmptyIdException::new);
        taskRepository.addTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Nullable
    @SneakyThrows
    public Project findProjectById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @Nullable val searchedProject = projectRepository.findById(userId, id);
        return Optional.ofNullable(searchedProject).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task findTaskById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @Nullable val searchedTask = taskRepository.findById(userId, id);
        return Optional.ofNullable(searchedTask).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findProjectTasksById(@Nullable String userId, @Nullable String projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        return taskRepository.findProjectTasksById(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void deleteProjectById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        projectRepository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    public void deleteProjectTasksById(@Nullable String userId, @Nullable String projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        taskRepository.deleteProjectTasksById(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void deleteProjectTaskById(@Nullable String userId, @Nullable String projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        taskRepository.deleteProjectTaskById(userId, projectId);
    }

}
