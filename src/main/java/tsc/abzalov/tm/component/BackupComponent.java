package tsc.abzalov.tm.component;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;

import static java.util.concurrent.TimeUnit.SECONDS;

public final class BackupComponent extends AbstractBackgroundTaskComponent {

    public BackupComponent(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void run() {
        loadBackup();
        scheduledBackupSave();
    }

    private void loadBackup() {
        executeBackupLoadCommand();
    }

    private void scheduledBackupSave() {
        getScheduledExecutorService()
                .scheduleWithFixedDelay(this::executeBackupSaveCommand, INITIAL_DELAY, BACKUP_DELAY, SECONDS);
    }

    private void executeBackupLoadCommand() {
        @NotNull val backupLoadCommandName = "backup-load";
        executeCommand(backupLoadCommandName);
    }

    private void executeBackupSaveCommand() {
        @NotNull val backupSaveCommandName = "backup-save";
        executeCommand(backupSaveCommandName);
    }

}
