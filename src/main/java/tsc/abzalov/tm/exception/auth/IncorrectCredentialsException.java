package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class IncorrectCredentialsException extends AbstractException {

    public IncorrectCredentialsException() {
        super("Credentials are incorrect! Please, try to login/register with correct credentials.");
    }

}
